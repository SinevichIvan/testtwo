const validate = (val, rule) => {
  let isValid;
  switch (rule) {
    case 'number':
      isValid = number(val);
      break;
    case 'string':
      isValid = string(val);
      break;
    case 'date':
      isValid = date(val);
      break;
    default:
      isValid = false;
  }
  return isValid;
};

const number = (val) => {
  switch (true) {
    case typeof val !== 'number':
      return false;
    default:
      return true;
  }
};


const string = (val) => {
  switch (true) {
    case typeof val !== 'string':
      return false;
    case val.length === 0:
      return false;
    default:
      return true;
  }
};

const date = (val) => {
  switch (true) {
    case val === 'Choose date':
      return false;
    case Date.parse(val) > Date.now():
      return false;
    default:
      return true;
  }
};

export default validate;
