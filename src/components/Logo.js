// @flow
import React from 'react';
import {
  Animated,
  StyleSheet,
  Platform,
  View,
} from 'react-native';
import {
  compose,
  lifecycle,
  withProps,
  withHandlers,
} from 'recompose';

type LogoProps = {
  heightContainer: any,
  heightImage: Number,
  widthImage: Number,
  animation: any,
};

const getOutputRange = (animation) => {
  let result = ['70%', '30%'];
  if (animation === 'up') {
    result = ['70%', '30%'];
  } else if (animation === 'down') {
    result = ['30%', '70%'];
  }
  return result;
};

const getImageSize = (animation) => {
  let result = 200;
  if (animation === 'up') {
    result = 200;
  } else if (animation === 'down') {
    result = 100;
  }
  return result;
};

const Logo = ({
  heightContainer,
  heightImage,
  widthImage,
  animation,
}: LogoProps) => {
  if (Platform.OS === 'ios') {
    return (
      <Animated.View
        style={{
          ...styles.container,
          height: heightContainer.interpolate({
            inputRange: [0, 1],
            outputRange: getOutputRange(animation),
          }),
        }}
      >
        <Animated.Image
          style={{
            width: heightImage,
            height: widthImage,
          }}
          source={require('../assets/rates.png')}
        />
      </Animated.View>
    );
  }
  return (
    <View
      style={{
        ...styles.container,
        height: animation === 'up' ? '40%' : '70%',
      }}
    >
      <Animated.Image
        style={{
          width: heightImage,
          height: widthImage,
        }}
        source={require('../assets/rates.png')}
      />
    </View>
  );
};


const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const enhancer = compose(
  withProps(({ animation }) => ({
    heightContainer: new Animated.Value(0),
    outputRange: getOutputRange(animation),
    heightImage: new Animated.Value(getImageSize(animation)),
    widthImage: new Animated.Value(getImageSize(animation)),
  })),
  withHandlers({
    runAnimationUp: ({ heightContainer, heightImage, widthImage }) => () => {
      Animated.timing(
        heightContainer,
        {
          toValue: 1,
          duration: 800,
        },
      ).start();
      Animated.timing(
        heightImage,
        {
          toValue: 100,
          duration: 800,
        },
      ).start();
      Animated.timing(
        widthImage,
        {
          toValue: 100,
          duration: 800,
        },
      ).start();
    },
    runAnimationDown: ({
      heightContainer,
      heightImage,
      widthImage,
      setAnimationState,
    }) => () => {
      Animated.timing(
        heightContainer,
        {
          toValue: 1,
          duration: 1000,
        },
      ).start(() => setAnimationState(false));
      Animated.timing(
        heightImage,
        {
          toValue: 200,
          duration: 1000,
        },
      ).start();
      Animated.timing(
        widthImage,
        {
          toValue: 200,
          duration: 1000,
        },
      ).start();
    },
  }),
  lifecycle({
    componentDidUpdate(prevProps) {
      if (this.props.animation === 'up' && this.props.animation !== prevProps.animation) {
        this.props.runAnimationUp();
      } else if (this.props.animation === 'down') {
        this.props.runAnimationDown();
      }
    },
  }),
);
export default enhancer(Logo);
