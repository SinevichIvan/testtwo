// @flow
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';
import {
  compose,
  withHandlers,
  withState,
} from 'recompose';


type CombineButtonProps = {
  handlerError: Function,
  buttons: any,
  width: String,
  error: any,
};

export const CombineButton = ({
  handlerError,
  buttons,
  width,
  error,
}: CombineButtonProps) => {
  const content = buttons.map((item) => {
    const result = React.cloneElement(
      item,
      { combineButton: handlerError },
    );
    return result;
  });
  return (
    <View style={{ ...styles.container, width }}>
      <View style={{
        ...styles.containerButton,
      }}
      >
        {content}
      </View>
      {error ? (
        <View style={styles.containerError}>
          <Text style={styles.textError}>{error}</Text>
        </View>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    marginTop: 30,
  },
  containerButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  containerError: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  textError: {
    color: 'red',
  },
});

const enhancer = compose(
  withState('error', 'setError', null),
  withHandlers({
    handlerError: ({ setError }) => (error) => {
      setError(error);
    },
  }),
);
export default enhancer(CombineButton);
