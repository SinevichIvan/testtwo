// @flow
import React from 'react';
import { View } from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import {
  compose,
  withHandlers,
} from 'recompose';

type DateTimePickerProps = {
  dateTimeVisible: Boolean,
  swithDateTimePicker: Function,
  handleDatePicked: Function,
};


const DateTimePickerComponent = ({
  dateTimeVisible,
  swithDateTimePicker,
  handleDatePicked,
}: DateTimePickerProps) => (
  <View style={{ flex: 1 }}>
    <DateTimePicker
      isVisible={dateTimeVisible}
      onConfirm={handleDatePicked}
      onCancel={swithDateTimePicker}
    />
  </View>
);


const enhancer = compose(
  withHandlers({
    handleDatePicked: ({ choseDate }) => (date) => {
      choseDate(date);
    },
  }),
);
export default enhancer(DateTimePickerComponent);
