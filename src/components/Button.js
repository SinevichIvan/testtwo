// @flow
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';


const getColor = (error, backgroundColor) => {
  let result;
  if (error) {
    result = 'red';
  } else {
    result = backgroundColor;
  }
  return result;
};


type ButtonProps = {
  data: Object,
  handlerButton: Function,
  width: Number,
  height: Number,
  backgroundColor: String,
  disabled: Boolean,
};

export const Button = ({
  data,
  handlerButton,
  width,
  height,
  backgroundColor,
  disabled,
}: ButtonProps) => (
  <TouchableOpacity
    onPress={handlerButton}
    disabled={disabled}
  >
    <View style={{
      width,
      height,
      backgroundColor: disabled ? '#858585' : getColor(data.error, backgroundColor),
      ...styles.container,
    }}
    >
      <Text>{data.value}</Text>
    </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
});
