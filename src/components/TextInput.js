// @flow
import React from 'react';
import {
  TextInput,
  StyleSheet,
  View,
  Text,
} from 'react-native';


type TextInputViewProps = {
  data: Object,
  handlerTextInput: Function,
  width: Number,
  height: Number,
};

export const TextInputView = ({
  data,
  handlerTextInput,
  width,
  height,
}: TextInputViewProps) => (
  <View style={{
    width,
    height,
    borderColor: data.error ? 'red' : 'black',
    ...styles.container,
  }}
  >
    <TextInput
      style={{
        ...styles.textInput,
      }}
      onChangeText={text => handlerTextInput(text)}
      value={data.value}
    />
    {data.error ? (
      <View style={styles.containerError}>
        <Text style={styles.textError}>{data.error}</Text>
      </View>
    ) : null}
  </View>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderRadius: 5,
  },
  textInput: {
    width: '100%',
    height: '100%',
    borderRadius: 5,
  },
  containerError: {
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 3,
  },
  textError: {
    color: 'red',
  },
});
