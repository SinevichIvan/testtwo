import axios from 'axios';

export const apiGetCurrencies = async () => {
  const res = await axios.get('http://www.nbrb.by/API/ExRates/Currencies');
  return res;
};

export const apiGetCurrencie = async (curId, date) => {
  const res = await axios.get(`http://www.nbrb.by/API/ExRates/Rates/${curId}?onDate=${date}`);
  return res;
};
