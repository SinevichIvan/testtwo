import {
  createStore,
  combineReducers,
  compose,
  applyMiddleware,
} from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import logger from 'redux-logger';
import storage from 'redux-persist/lib/storage';
import CoreReducer from '../modules/core/coreReducer';


const rootReducer = combineReducers({
  core: CoreReducer,
});

const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['core'],
};

const pReducer = persistReducer(persistConfig, rootReducer);

const composeEnhancers = compose;


export const store = createStore(
  pReducer,
  composeEnhancers(applyMiddleware(thunk, logger)),
);

export const persistor = persistStore(store);
