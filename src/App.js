// @flow
import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from './modules/core/homeScreen/HomeScreenContainer';
import DetailsScreenView from './modules/core/detailsScreen/DetailsScreenContainer';
import AllCurrenciesScreenView from './modules/core/allCurrenciesScreen/AllCurrenciesScreenContainer';


const App = () => {
  const HomeStack = createStackNavigator(
    {
      Home: {
        screen: HomeScreen,
        navigationOptions: () => ({
          gesturesEnabled: false,
        }),
      },
      AllCurrencies: {
        screen: AllCurrenciesScreenView,
        navigationOptions: () => ({
          gesturesEnabled: false,
        }),
      },
    },
    {
      initialRouteName: 'Home',
      headerMode: 'none',
      mode: 'modal',
    },
  );

  const RootStack = createStackNavigator(
    {
      Home: HomeStack,
      Details: {
        screen: DetailsScreenView,
        navigationOptions: () => ({
          gesturesEnabled: false,
        }),
      },
    },
    {
      initialRouteName: 'Home',
      headerMode: 'none',
      mode: 'card',
    },
  );


  const AppContainer = createAppContainer(RootStack);
  return <AppContainer />;
};

export default App;
