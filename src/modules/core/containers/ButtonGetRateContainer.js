import { connect } from 'react-redux';
import {
  compose,
  withHandlers,
} from 'recompose';
import { createSelector } from 'reselect';
import { Button } from '../../../components/Button';
import {
  getRateHandler,
} from '../coreReducer';

/* reselect Memoize */
const getSelectFieldReducer = (state, props) => state.core[props.field];

const makeGetField = () => createSelector(
  [getSelectFieldReducer],
  selectField => selectField,
);
/* --------- */

const makeMapStateToProps = () => {
  const getSelectField = makeGetField();
  const mapStateToProps = (state, props) => ({
    state: state.core,
    data: getSelectField(state, props),
  });
  return mapStateToProps;
};

const mapDispatchToProps = dispatch => ({
  getRateHandlerDispatch: (state, navigation) => dispatch(getRateHandler(state, navigation)),
});

export default compose(
  connect(
    makeMapStateToProps,
    mapDispatchToProps,
  ),
  withHandlers({
    handlerButton: ({ getRateHandlerDispatch, state, navigation }) => async () => {
      await getRateHandlerDispatch(state, navigation);
    },
  }),
)(Button);
