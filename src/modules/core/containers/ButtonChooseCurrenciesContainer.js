import { connect } from 'react-redux';
import {
  compose,
  withHandlers,
  lifecycle,
} from 'recompose';
import { createSelector } from 'reselect';
import { Button } from '../../../components/Button';
import {
  setValue,
  swithDateTimePicker,
  setDate,
} from '../coreReducer';

/* reselect Memoize */
const getSelectFieldReducer = (state, props) => state.core[props.field];

const makeGetField = () => createSelector(
  [getSelectFieldReducer],
  selectField => selectField,
);
/* --------- */

const makeMapStateToProps = () => {
  const getSelectField = makeGetField();
  const mapStateToProps = (state, props) => ({
    data: getSelectField(state, props),
  });
  return mapStateToProps;
};

const mapDispatchToProps = dispatch => ({
  setValueDispath: value => dispatch(setValue(value)),
  swithDateTimePickerDispatch: () => dispatch(swithDateTimePicker()),
  setDateDispatch: date => dispatch(setDate(date)),
});

export default compose(
  connect(
    makeMapStateToProps,
    mapDispatchToProps,
  ),
  withHandlers({
    handlerButton: ({ navigation }) => () => {
      navigation.navigate('AllCurrencies');
    },
  }),
  lifecycle({
    componentDidUpdate(prevProps) {
      if (this.props.combineButton && this.props.data.error !== prevProps.data.error) {
        this.props.combineButton(this.props.data.error);
      }
    },
  }),
)(Button);
