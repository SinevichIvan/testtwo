import { connect } from 'react-redux';
import {
  compose,
  withHandlers,
} from 'recompose';
import { createSelector } from 'reselect';
import { TextInputView } from '../../../components/TextInput';
import { setValue } from '../coreReducer';

/* reselect Memoize */
const getSelectFieldReducer = (state, props) => state.core[props.field];

const makeGetField = () => createSelector(
  [getSelectFieldReducer],
  selectField => selectField,
);
/* --------- */

const makeMapStateToProps = () => {
  const getSelectField = makeGetField();
  const mapStateToProps = (state, props) => ({
    data: getSelectField(state, props),
  });
  return mapStateToProps;
};

const mapDispatchToProps = dispatch => ({
  setValueDispath: value => dispatch(setValue(value)),
});

export default compose(
  connect(
    makeMapStateToProps,
    mapDispatchToProps,
  ),
  withHandlers({
    handlerTextInput: ({ setValueDispath, data, field }) => (value) => {
      const reduxValue = {
        ...data,
        field,
        value,
        error: null,
      };
      setValueDispath(reduxValue);
    },
  }),
)(TextInputView);
