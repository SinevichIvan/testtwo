import { Keyboard, Platform } from 'react-native';
import { connect } from 'react-redux';
import {
  compose,
  lifecycle,
  withHandlers,
  withState,
} from 'recompose';
import { HomeScreenView } from './HomeScreenView';
import {
  getAllCurrencies,
  swithDateTimePicker,
  setDate,
} from '../coreReducer';

export default compose(
  connect(
    state => ({
      dateTimePicker: state.core.dateTimePicker,
      date: state.core.date,
      allCurrencies: state.core.allCurrencies,
      stateDataGetting: state.core.stateDataGetting,
      state: state.core,
    }),
    dispatch => ({
      getAllCurrencies: () => dispatch(getAllCurrencies()),
      swithDateTimePickerDispatch: () => dispatch(swithDateTimePicker()),
      setDateDispatch: date => dispatch(setDate(date)),
    }),
  ),
  withState('animation', 'setAnimationState', false),
  withHandlers({
    keyboardDidShowListener: () => () => {
    },
    keyboardDidHideListener: () => () => {
    },
    keyboardDidShow: ({ setAnimationState }) => () => {
      setAnimationState('up');
    },
    keyboardDidHide: ({ setAnimationState }) => () => {
      setAnimationState('down');
    },
    keyboardDismiss: () => () => {
      Keyboard.dismiss();
    },
  }),
  withHandlers({
    choseDate: ({ date, setDateDispatch, swithDateTimePickerDispatch }) => (value) => {
      const dateParser = new Date(value);
      swithDateTimePickerDispatch();
      const reduxValue = {
        ...date,
        error: null,
        value: `${dateParser.getFullYear()}-${dateParser.getMonth()}-${dateParser.getDate()}`,
      };
      setDateDispatch(reduxValue);
    },
    setCurId: ({ setCurIdDispatch, swithDateTimePickerDispatch }) => (curId) => {
      setCurIdDispatch(curId);
      swithDateTimePickerDispatch();
    },
  }),
  lifecycle({
    componentDidMount() {
      this.props.getAllCurrencies();
      this.props.keyboardDidShowListener = Keyboard.addListener(
        Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
        this.props.keyboardDidShow,
      );
      this.keyboardDidHideListener = Keyboard.addListener(
        'keyboardDidHide',
        this.props.keyboardDidHide,
      );
    },
    componentWillUnmount() {
      this.props.keyboardDidShowListener.remove();
      this.props.keyboardDidHideListener.remove();
    },
  }),
)(HomeScreenView);
