// @flow
import React from 'react';
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
} from 'react-native';
import DateTimePickerComponent from '../../../components/DataTimePicker';
import Logo from '../../../components/Logo';
import TextInputView from '../containers/TextInputContainer';
import ButtonDate from '../containers/ButtonChooseDateContainer';
import ButtonCurrencies from '../containers/ButtonChooseCurrenciesContainer';
import ButtonGetRate from '../containers/ButtonGetRateContainer';
import CombineButton from '../../../components/CombineButton';


type HomeScreenViewProps = {
  dateTimePicker: Boolean,
  swithDateTimePickerDispatch: Function,
  choseDate: Function,
  state: Object,
  setAnimationState: Function,
  animation: any,
  keyboardDismiss: Function,
  navigation: any,
};

export const getDisabled = (state: any, animation: any) => {
  let result = false;
  if (
    state.name.error
    || state.date.error
    || state.chooseCurrency.error
    || animation === 'up'
  ) {
    result = true;
  }
  return result;
};

export const HomeScreenView = ({
  dateTimePicker,
  swithDateTimePickerDispatch,
  choseDate,
  state,
  setAnimationState,
  animation,
  keyboardDismiss,
  navigation,
}: HomeScreenViewProps) => (
  <TouchableWithoutFeedback onPress={keyboardDismiss}>
    <View style={{ ...styles.container }}>
      <Logo
        animation={animation}
        setAnimationState={setAnimationState}
      />
      <View style={styles.containerSectionInput}>
        <TextInputView
          width={250}
          height={40}
          field="name"
        />
        <CombineButton
          buttons={[
            <ButtonDate
              width={100}
              height={40}
              backgroundColor="#247BA0"
              field="date"
              key={0}
              disabled={animation === 'up'}
            />,
            <ButtonCurrencies
              width={100}
              height={40}
              backgroundColor="#247BA0"
              field="chooseCurrency"
              navigation={navigation}
              key={2}
              disabled={animation === 'up'}
            />,
          ]}
          width={300}
        />
        <View style={{ marginTop: 20 }}>
          <ButtonGetRate
            width={200}
            height={40}
            backgroundColor="#F3FFBD"
            navigation={navigation}
            field="getRate"
            disabled={getDisabled(state, animation)}
          />
        </View>
      </View>
      <DateTimePickerComponent
        dateTimeVisible={dateTimePicker}
        swithDateTimePicker={swithDateTimePickerDispatch}
        choseDate={choseDate}
      />
    </View>
  </TouchableWithoutFeedback>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    flexDirection: 'column',
    borderWidth: 1,
  },
  containerSectionInput: {
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
});
