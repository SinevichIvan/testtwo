import { connect } from 'react-redux';
import {
  compose,
  withState,
  withHandlers,
  lifecycle,
} from 'recompose';
import { State } from 'react-native-gesture-handler';
import { DetailsScreenView } from './DetailsScreenView';
import { getCurrencie } from '../coreReducer';

export default compose(
  connect(
    state => ({
      state: state.core,
      stateDataGetting: state.core.stateDataGetting,
      date: state.core.date,
      curId: state.core.curId,
      currency: state.core.currency,
    }),
    dispatch => ({
      getCurrencie: (curId, date) => dispatch(getCurrencie(curId, date)),
    }),
  ),
  withState('cap', 'setCap', false),
  withHandlers({
    handleStateChange: ({ navigation }) => ({ nativeEvent }) => {
      if (nativeEvent.state === State.ACTIVE) {
        navigation.navigate('Home');
      }
    },
  }),
  lifecycle({
    componentDidMount() {
      this.props.getCurrencie(
        this.props.curId,
        this.props.date,
      );
    },
  }),
)(DetailsScreenView);
