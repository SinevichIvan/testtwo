// @flow
/* eslint no-console: "off" */
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { PanGestureHandler } from 'react-native-gesture-handler';
import { Spinner } from '../../../components/Spinner';
import validate from '../../../validation/Validation';

type DetailsScreenViewProps = {
  handleStateChange: Function,
  stateDataGetting: any,
  currency: any,
};

export const DetailsScreenView = ({
  handleStateChange,
  stateDataGetting,
  currency,
}: DetailsScreenViewProps) => {
  let content;
  if (stateDataGetting) {
    content = <Spinner />;
  } else if (
    currency
    && currency.data
    && validate(currency.data.Cur_Abbreviation, 'string')
    && validate(currency.data.Cur_ID, 'number')
    && validate(currency.data.Cur_Name, 'string')
    && validate(currency.data.Cur_OfficialRate, 'number')
    && validate(currency.data.Cur_Scale, 'number')
    && validate(currency.data.Date, 'string')
  ) {
    content = (
      <View>
        <Text style={styles.text}>
          {`Abbreviation: ${currency.data.Cur_Abbreviation}`}
        </Text>
        <Text style={styles.text}>
          {`ID:  ${currency.data.Cur_ID}`}
        </Text>
        <Text style={styles.text}>
          {`Name:  ${currency.data.Cur_Name}`}
        </Text>
        <Text style={styles.text}>
          {`OfficialRate:  ${currency.data.Cur_OfficialRate}`}
        </Text>
        <Text style={styles.text}>
          {`Scale:  ${currency.data.Cur_Scale}`}
        </Text>
        <Text style={styles.text}>
          {`Date:  ${currency.data.Date}`}
        </Text>
      </View>
    );
  } else {
    content = <Text>Data not found</Text>;
  }
  return (
    <PanGestureHandler minPointers={2} activeOffsetX={40} onHandlerStateChange={handleStateChange}>
      <View style={styles.container}>
        {content}
      </View>
    </PanGestureHandler>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 20,
  },
});
