/* eslint no-console: "off" */
import { apiGetCurrencies, apiGetCurrencie } from '../../service/nbrb';
import validate from '../../validation/Validation';


const initialState = {
  stateDataGetting: null,
  allCurrencies: null,
  currency: null,
  dateTimePicker: false,
  curId: null,
  date: { value: 'Choose date', error: null },
  name: { value: null, error: null, rule: 'name' },
  getRate: { value: 'getRate', error: null },
  chooseCurrency: { value: 'Choose currency', error: null },
  tryGetRate: false,
  modeScreen: 'modal',
};


export const getRateHandler = (state, navigation) => async (dispatch) => {
  if (!state.name.value) {
    await dispatch(setValue({
      ...state.name,
      field: 'name',
      error: 'empty name field',
    }));
  } else if (!validate(state.date.value, 'date')) {
    await dispatch(setValue({
      ...state.date,
      field: 'date',
      error: 'invalid Date',
    }));
  } else if (state.chooseCurrency.value === 'Choose currency') {
    await dispatch(setValue({
      ...state.chooseCurrency,
      field: 'chooseCurrency',
      error: 'invalid Currency',
    }));
  } else {
    await dispatch(getCurrencie(state.curId, state.date));
    navigation.navigate('Details');
  }
};


export const getAllCurrencies = () => async (dispatch) => {
  await dispatch(setStateDataGetting(true));
  apiGetCurrencies()
    .then((result) => {
      dispatch(setAllCurrencies(result));
      dispatch(setStateDataGetting(null));
    })
    .catch((error) => {
      console.log(error);
      dispatch(setStateDataGetting(false));
    });
};

export const getCurrencie = (curId, date) => async (dispatch) => {
  dispatch(setStateDataGetting(true));
  apiGetCurrencie(`${curId}`, date)
    .then((result) => {
      dispatch(setStateDataGetting(null));
      dispatch(setCurrency(result));
    })
    .catch((error) => {
      console.log(error);
      dispatch(setStateDataGetting(false));
    });
};


const SET_VALUE = 'SET_VALUE';
const SET_ALL_CURRENCIES = 'SET_ALL_CURRENCIES';
const SET_STATE_DATA_GETTING = 'SET_STATE_DATA_GETTING';
const SWITH_DATE_TIME_PICKER = 'SWITH_DATE_TIME_PICKER';
const SET_DATE = 'SET_DATE';
const SET_CUR_ID = 'SET_CUR_ID';
const SET_CHOOSE_CURRENCY = 'SET_CHOOSE_CURRENCY';
const SET_CURRENCY = 'SET_CURRENCY';
const SET_MODE_SCREEN = 'SET_MODE_SCREEN';


export const setValue = value => ({
  type: SET_VALUE,
  payload: value,
});

export const setAllCurrencies = value => ({
  type: SET_ALL_CURRENCIES,
  payload: value,
});

export const setCurrency = value => ({
  type: SET_CURRENCY,
  payload: value,
});

export const setCurId = value => ({
  type: SET_CUR_ID,
  payload: value,
});

export const setChooseCurrency = value => ({
  type: SET_CHOOSE_CURRENCY,
  payload: value,
});

export const setStateDataGetting = value => ({
  type: SET_STATE_DATA_GETTING,
  payload: value,
});

export const setDate = value => ({
  type: SET_DATE,
  payload: value,
});

export const swithDateTimePicker = value => ({
  type: SWITH_DATE_TIME_PICKER,
  payload: value,
});

export const setModeScreen = value => ({
  type: SET_MODE_SCREEN,
  payload: value,
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_VALUE:
      return {
        ...state,
        [action.payload.field]: action.payload,
      };
    case SET_CUR_ID:
      return {
        ...state,
        curId: action.payload,
      };
    case SET_CURRENCY:
      return {
        ...state,
        currency: action.payload,
      };
    case SET_DATE:
      return {
        ...state,
        date: action.payload,
      };
    case SWITH_DATE_TIME_PICKER:
      return {
        ...state,
        dateTimePicker: !state.dateTimePicker,
      };
    case SET_ALL_CURRENCIES:
      return {
        ...state,
        allCurrencies: action.payload,
      };
    case SET_STATE_DATA_GETTING:
      return {
        ...state,
        stateDataGetting: action.payload,
      };
    case SET_CHOOSE_CURRENCY:
      return {
        ...state,
        chooseCurrency: action.payload,
      };
    case SET_MODE_SCREEN:
      return {
        ...state,
        modeScreen: action.payload,
      };
    default: return state;
  }
};
export default reducer;
