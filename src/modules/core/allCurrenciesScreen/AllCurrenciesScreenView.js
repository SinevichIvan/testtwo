// @flow
/* eslint no-console: "off" */
import React from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Text,
} from 'react-native';
import validate from '../../../validation/Validation';

type AllCurrenciesScreenViewProps = {
  renderItem: any,
  allCurrencies: any,
};

export const AllCurrenciesScreenView = ({
  renderItem,
  allCurrencies,
}: AllCurrenciesScreenViewProps) => {
  const content = allCurrencies ? allCurrencies.data.filter((item) => {
    if (validate(item.Cur_ID, 'number')
         && item.Cur_ID >= 290
         && item.Cur_ID <= 300
         && validate(item.Cur_Name, 'string')
    ) {
      return true;
    }
    return false;
  }) : null;
  return (
    <View style={styles.container}>
      {content ? (
        <FlatList
          data={content}
          renderItem={renderItem}
        />
      ) : (
        <View>
          <Text>Data not found. There may be no internet connection.</Text>
        </View>
      )
      }
    </View>
  );
};

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 80,
  },
  renderItem: {
    width: '100%',
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: (0, 10, 0, 10),
    borderBottomColor: 'black',
    borderBottomWidth: 2,
  },
});
