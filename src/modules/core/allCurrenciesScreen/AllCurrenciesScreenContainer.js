/* eslint react/prop-types: "off" */
import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import { connect } from 'react-redux';
import {
  compose,
  withHandlers,
} from 'recompose';
import { AllCurrenciesScreenView, styles } from './AllCurrenciesScreenView';
import { getAllCurrencies, setCurId, setChooseCurrency } from '../coreReducer';

export default compose(
  connect(
    state => ({
      allCurrencies: state.core.allCurrencies,
    }),
    dispatch => ({
      getAllCurrencies: () => dispatch(getAllCurrencies()),
      setCurIdDispatch: curId => dispatch(setCurId(curId)),
      setChooseCurrencyDispath: curAbbrev => dispatch(setChooseCurrency(curAbbrev)),
    }),
  ),
  withHandlers({
    setCurId: ({ setCurIdDispatch, setChooseCurrencyDispath, navigation }) => (curId, curAbbrev) => {
      setCurIdDispatch(curId);
      setChooseCurrencyDispath({
        value: curAbbrev,
        error: null,
      });
      navigation.navigate('Home');
    },
  }),
  withHandlers({
    renderItem: props => ({ item }) => (
      <TouchableOpacity
        onPress={() => props.setCurId(item.Cur_ID, item.Cur_Abbreviation)}
        style={styles.renderItem}
      >
        <Text>{item.Cur_Abbreviation}</Text>
        <Text>{item.Cur_Name}</Text>
      </TouchableOpacity>
    ),
  }),
)(AllCurrenciesScreenView);
